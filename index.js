const express = require('express')
var cors = require('cors')
const bodyParser = require('body-parser');
const BitcoinC = require("./services/bitcoin")
var firebase = require('firebase/app');
const Constants = require("./services/constants")
var rootFB = require("firebase")
const fs = require('fs');
require('firebase/auth');
require('firebase/database');


const app = express()
app.use(cors())

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());
app.use(bodyParser.text());
var jsonParser = bodyParser.json()

let appFirebase
if (!firebase.apps.length) {
    appFirebase = firebase.initializeApp({
        apiKey: "AIzaSyCQxHCf9-5lU-E1lSqyfHMSQBK5tMe2rMM",
        authDomain: "adncrypto-8f596.firebaseapp.com",
        databaseURL: "https://adncrypto-8f596-default-rtdb.firebaseio.com",
        projectId: "adncrypto-8f596",
        storageBucket: "adncrypto-8f596.appspot.com",
        messagingSenderId: "101011996067",
        appId: "1:101011996067:web:74f04008818ab43e98ae16",
        measurementId: "G-B6PPL041W0",
    })
} else {
    appFirebase = firebase.app() // if already initialized, use that one
}

const bitcoin = new BitcoinC(appFirebase)


const port = 3001

app.get('/', (req, res) => {
    res.send('Hello World!')
})

app.post('/generarWallet', jsonParser, async (req, res) => {
    const {userID, facturaID} = req.body
    console.log("el pac", packageID, userID)
    const nuevaWallet = await bitcoin.obtenerWallet(userID, packageID)
    res.send({exito: 1, nuevaWallet})
})

app.post('/makePackageRequest', jsonParser, async (request, response) => {
    console.log("vamos a iniciar make package", request.body)
    let { userID, packageID, tipoPlan } = request.body
    if (!userID || !packageID || !tipoPlan) {
        response.send({ success: 0, error: "All parameters are required" })
        return
    }
    tipoPlan = parseInt(tipoPlan)
    try {
        let snapshot = await appFirebase
            .database()
            .ref("usuarios/" + userID)
            .once("value")

        const actualUser = snapshot.val()

        if (!actualUser) {
            response.send({ success: 0, error: "User doesn´t exist" })
            return
        }

        if (actualUser.pendingInvoice) {
            response.send({ success: 0, error: "User already has a pending invoice" })
            return
        }

        snapshot = await appFirebase
            .database()
            .ref("packages/" + packageID)
            .once("value")

        const requestedPackage = snapshot.val()

        if (!requestedPackage) {
            response.send({ success: 0, error: "Package doesn´t exist" })
            return
        }
        let selectedPrice = tipoPlan == 0 ? requestedPackage.price : tipoPlan == 1 ? requestedPackage.trimPrice 
            : tipoPlan == 2 ? requestedPackage.semPrice : requestedPackage.anualPrice
        let selectedDiscount = tipoPlan == 0 ? 0 : tipoPlan == 1 ? requestedPackage.trimDisc 
            : tipoPlan == 2 ? requestedPackage.semDisc : requestedPackage.anualDisc
        const precioBTC = await bitcoin.precioCompraBTC()
        const apagarBTC = (selectedPrice / precioBTC).toFixed(8)

        const timestampFactura = rootFB.database.ServerValue.TIMESTAMP
        let factura = {
            packageID,
            amount: selectedPrice,
            apagarBTC,
            discount: selectedDiscount,
            totalPrice : (selectedPrice + selectedDiscount),
            subject: `Registro a membresía ${requestedPackage.name}`,
            pending: true,
            tipoPlan,
            paid: false,
            cancelled: false,
            status: "Pendiente",
            createdAt: timestampFactura,
        }
        const newKey = appFirebase.database().ref().push().key;

        if(actualUser.derivateAsignado){
            let walletAsignada = await bitcoin.walletAsignada(userID, newKey, apagarBTC, actualUser.derivateAsignado, actualUser.walletAsignada, timestampFactura)
            factura.wallet = walletAsignada.wallet
        } else {
            let nuevaWallet = await bitcoin.obtenerNuevaWallet(userID, newKey, apagarBTC, timestampFactura)
            factura.wallet = nuevaWallet.wallet
        }

        const newInvoice = await appFirebase
            .database()
            .ref("facturas/"+userID+"/"+newKey)
            .set(factura)

        await appFirebase
            .database()
            .ref("usuarios")
            .update({
                [userID + "/pendingInvoice"]: newKey,
            })
        response.send({ success: 1, facturaID: newKey })
    } catch (error) {
        console.log("Error on makeAPackageRequest", error.message)
        response.send({ success: 0, error: error.message })
    }
})

var Server;
if(Constants.production){
    const options = {
        key: fs.readFileSync(__dirname + '/privkey.pem', 'utf8'),
        cert: fs.readFileSync(__dirname + '/cert.pem', 'utf8')
    };
    Server = require('https').createServer(options, app); 
} else {
    Server = require('http').createServer(app);
}

//var Server = require('http').createServer(app);

Server.listen(port, () => {
    console.log("corriendo")
})