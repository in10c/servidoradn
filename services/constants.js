const bitcoinjs = require('bitcoinjs-lib')

const Constants = {
    production: false,
    network: "testnet",
    tiempoExpiraFacturas: 60,
    pagoPorcentajeReferidos: .35,
    btcExplorer: ()=>{
        if(Constants.network === "mainnet"){
            return 'https://blockstream.info/tx'
        } else {
            return 'https://blockstream.info/testnet/tx'
        }
    },
    
    globalNetwork :()=> Constants.network === "mainnet" ? bitcoinjs.networks.bitcoin : bitcoinjs.networks.testnet,
    blockstream :()=> Constants.network === "mainnet" ? "https://blockstream.info" : "https://blockstream.info/testnet",
    websocketURL :()=> Constants.network === "mainnet" ? "wss://ws.blockchain.info/inv" : "https://testnet.blockchain.info/",
};

module.exports = Constants