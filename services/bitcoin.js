const dotenv = require('dotenv');
const bitcoinjs = require("bitcoinjs-lib");
const bip32 = require('bip32');
var axios = require("axios").default
const bip39 = require('bip39');
const ccxt = require ('ccxt');
const Constants = require("./constants")
const networkToUse = Constants.network === "mainnet" ? bitcoinjs.networks.bitcoin :  bitcoinjs.networks.testnet
const blockstream = Constants.network === "mainnet" ? "https://blockstream.info" : "https://blockstream.info/testnet"
dotenv.config();

class Bitcoin {
    constructor(firebase){
        this.firebase = firebase
        this.rootKey = this.obtenerXPUB()
        this.coinbase = new ccxt.coinbase()
        this.obtenerConfigServ()
    }
    async precioCompraBTC(){
        const price = await this.coinbase.fetchTicker("BTC/EUR")
        return price.ask
    }
    async obtenerNuevaWallet(userID, facturaID, apagarBTC, timestampFactura){
        let snap = await this.firebase.database().ref("/appSettings/direccionesEnEspera").limitToFirst(1).once("value")
        let direccionesPendientes = snap.val()
        console.log("las direcciones pendientes son", direccionesPendientes)
        let numberofunused = this.getNumberOfUnused(direccionesPendientes)
        console.log("el numero total de dir activas son", numberofunused)
        if(!direccionesPendientes){
            if(numberofunused >= 20){
                throw new Error("Por ahora no podemos generar más direcciones sin usar, por favor inténtelo dentro de unos minutos.")
            } else {
                await this.generarDireccion(userID, facturaID, apagarBTC, timestampFactura)
            }
        } else {
            let keyObj = Object.keys(direccionesPendientes)[0]
            console.log("si hay ddir", direccionesPendientes)
            await this.reusarDireccion(userID, direccionesPendientes[keyObj], facturaID, apagarBTC, keyObj, timestampFactura)
        }
        return this.direccionesActivas[userID]
    }
    getNumberOfUnused(direccionesPendientes){
        let numDirPend = direccionesPendientes && Object.keys(direccionesPendientes).length > 0 ? Object.keys(direccionesPendientes).length : 0
        let numDirActi = this.direccionesActivas && Object.keys(this.direccionesActivas).length > 0 ? Object.keys(this.direccionesActivas).length : 0
        console.log("pendientes", numDirPend, "activas", numDirActi)
        return numDirActi + numDirPend
    }
    async walletAsignada(userID, facturaID, apagarBTC, derivate, wallet, timestampFactura){
        this.direccionesActivas[userID] = {
            derivate, wallet, facturaID, apagarBTC, newOne: false, timestampFactura
        }
        await this.firebase.database().ref("/appSettings/direccionesActivas").set(this.direccionesActivas)
        return this.direccionesActivas[userID]
    }

    async generarDireccion(userID, facturaID, apagarBTC, timestampFactura){
        const derivate = "m/0'/0/"+this.counterDerivate
        const address1 = this.getAddress(this.rootKey.derivePath(derivate))

        await this.addActiveDirections(derivate, userID, address1, facturaID, apagarBTC, timestampFactura)
        console.log("counter", this.counterDerivate)
    }
    async reusarDireccion(userID, direccionesPendientes, facturaID, apagarBTC, keyObj, timestampFactura){
        const {derivate, wallet} = direccionesPendientes
        this.direccionesActivas[userID] = {
            derivate, wallet, facturaID, apagarBTC, newOne: true, timestampFactura
        }
        await this.firebase.database().ref("/appSettings/direccionesActivas").set(this.direccionesActivas)
        await this.firebase.database().ref("/appSettings/direccionesEnEspera/"+keyObj).set(null)
    }

    async addActiveDirections(_derivate, _userID, _address1, facturaID, apagarBTC, timestampFactura){
        this.direccionesActivas[_userID] = {
            derivate: _derivate, wallet: _address1, facturaID, apagarBTC, newOne: true, timestampFactura
        }
        this.counterDerivate += 1
        await this.firebase.database().ref("/appSettings/direccionesActivas").set(this.direccionesActivas)
        await this.firebase.database().ref("/appSettings/counterDerivate").set(this.counterDerivate)
    }
    obtenerConfigServ(){
        this.firebase.database().ref("/appSettings/direccionesActivas").on("value", snap=>{
            this.direccionesActivas = snap.val() ? snap.val() : {}
        })
        this.firebase.database().ref("/appSettings/counterDerivate").on("value", snapB=>{
            this.counterDerivate = snapB.val() ? snapB.val() : 0
        })
        setTimeout(this.openServer.bind(this), 2000)
    }
    obtenerXPUB(){
        const mnemonic = Constants.production ? process.env.MNMEMONITPROD : process.env.MNMEMONICTEST;
        const seed = bip39.mnemonicToSeedSync(mnemonic);
        const node = bip32.fromSeed(seed);
        return node;
    }
    getAddress(node) {
        return bitcoinjs.payments.p2pkh({ pubkey: node.publicKey, network: networkToUse }).address
    }
    async openServer(){
        let indices = Object.keys(this.direccionesActivas)
        for (let index = 0; index < indices.length; index++) {
            const userID = indices[index]
            const {wallet, apagarBTC, facturaID, derivate, timestampFactura, newOne} = this.direccionesActivas[userID]
            const utxo = await axios.get(blockstream+'/api/address/' + wallet+"/utxo")
            const confirmed = utxo.data && utxo.data.filter(val=>val.status.confirmed)
            //si hay algunos confirmados, procedemos a registrarlo
            console.log("los confirmados son ", confirmed, "el usuario es ", userID)
            if(confirmed && confirmed.length > 0){
                console.log("entro a confirmed")
                this.marcarComoPagado(userID, confirmed, apagarBTC, facturaID, derivate, wallet)
            } 
            this.comprobarSiExpiro(userID, wallet, facturaID, derivate, newOne, timestampFactura)
        }
        setTimeout(this.openServer.bind(this), 10000)   
    }

    async comprobarSiExpiro(userID, wallet, facturaID, derivate, newOne,timestampFactura){
        let tiempoPasado = (new Date().getTime()) - timestampFactura
        let seconds = tiempoPasado / 1000
        let minutes = seconds / 60
        let restante = (minutes).toString().split(".")
        let minutosPasadosEntero = parseInt(restante[0])
        console.log("comprobando si expiro", minutosPasadosEntero, Constants.tiempoExpiraFacturas)
        if(minutosPasadosEntero >= Constants.tiempoExpiraFacturas){
            await this.expirarFactura(userID, wallet, facturaID, derivate, newOne)
        }
    }

    async expirarFactura(userID, wallet, facturaID, derivate, _newOne){

        await this.firebase.database().ref("appSettings/direccionesActivas/"+userID).set(null)
        if(_newOne){ 
            await this.firebase.database().ref("appSettings/direccionesEnEspera/").push({
                derivate, wallet 
            })
        }
        await this.firebase.database().ref("facturas/"+userID+"/"+facturaID)
            .update({
                pending: false,
                paid: false,
                cancelled: true,
                status: "Expirada",
                canceledTime: new Date().getTime()
            })
        await this.firebase
            .database()
            .ref("usuarios")
            .update({
                [userID + "/pendingInvoice"]: null,
            })
    }

    async marcarComoPagado(userID, txBlockchain, apagarBTC, facturaID, derivate, wallet){
        let snap = await this.firebase.database().ref("/transacciones/"+userID).once("value")
        let txs = snap.val()
        let txConfirmed
        //si hay txs pasadas las quitamos de los confirmados 
        if(txs){
            //console.log("paso al lado en que si hay txs antiguas", txs, txBlockchain)
            txConfirmed = this.obtenerTXIDPasados(txs, txBlockchain)
            console.log("como quedaron", txConfirmed)
        } else {
            txConfirmed = txBlockchain
        }
        let pagado = 0
        let payments = []
        for (let index = 0; index < txConfirmed.length; index++) {
            let elemento = txConfirmed[index];
            pagado += elemento.value
            payments.push({
                txid: elemento.txid,
                tiempo: elemento.status.block_time,
                valor: elemento.value,
                facturaID
            })
        }
        let apagarSatoshis = parseInt(parseFloat(apagarBTC) * 100000000)
        console.log("lo pagado es mas o igual", pagado, apagarSatoshis, apagarBTC, parseFloat(apagarBTC))
        let payDate = new Date().getTime()
        if(pagado >= apagarSatoshis){
            console.log("si paso a marcarse como pagado")
            for (let indice = 0; indice < payments.length; indice++) {
                await this.firebase.database().ref("/transacciones/"+userID).push(payments[indice])
            }
            await this.firebase.database().ref("/facturas/"+userID+"/"+facturaID).update({
                pending: false, paid: true, status: "Pagado", payDate, payments
            })
            await this.firebase.database().ref("/appSettings/direccionesActivas/"+userID).set(null)

            let snapFac = await this.firebase.database().ref("/facturas/"+userID+"/"+facturaID).once("value")
            let factura = snapFac.val()
            await this.firebase.database().ref("/usuarios/"+userID).update({
                walletAsignada: wallet,
                derivateAsignado: derivate,
                pendingInvoice: null,
                licenciaADN: parseInt(factura.packageID),
                tipoPlan: factura.tipoPlan
            })
            await this.revisarSponsorYPagar(userID, apagarSatoshis, payDate)
        }
    }
    async revisarSponsorYPagar(userID, apagarSatoshis, payDate){
        let sato = apagarSatoshis * Constants.pagoPorcentajeReferidos
        let monto = sato / 100000000
        let snap = await this.firebase.database().ref("/path/"+userID).once("value")
        let path = snap.val()
        if(path && Array.isArray(path) && path.length > 0){
            const sponsorID = path[(path.length - 1)]
            await this.firebase.database().ref("/pagos/"+sponsorID).push({
                descripcion: "Pago por referido - Actualización de licencia",
                monto,
                fecha: payDate,
                estado: "Pendiente"
            })
        }
    }
    obtenerTXIDPasados(txs, txConfirmed){
        let indices = Object.keys(txs)
        let indicesAntiguos = []
        let pasan = []
        for (let index = 0; index < indices.length; index++) {
            indicesAntiguos.push(txs[indices[index]].txid)
        }
        console.log("los indices antiguos son", txs, indices, indicesAntiguos)
        for (let indiceTX = 0; indiceTX < txConfirmed.length; indiceTX++) {
            const txConfirmada = txConfirmed[indiceTX];
            if(!indicesAntiguos.includes(txConfirmada.txid)){
                console.log("si paso a los q pasaron")
                pasan.push(txConfirmada)
            }
        }
        return pasan
    }
}

module.exports = Bitcoin;